import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './pages/home';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Play from './Games/game123/play';
import rootReducer from './reducer';
import classic from './image/classic.PNG'
import xso from './image/xso.PNG'
import win from './image/win.PNG'
import jig from './image/jig.PNG'
import killer from './image/killer.PNG'
import Jigsaw from './Games/jigsaws/jigsaw';
import Killer from './Games/jigsaws/killer';


const finalCreateStore = compose(
    applyMiddleware(thunk),
	window.devToolsExtension ? window.devToolsExtension() : f => f
    )(createStore);
    
const store = finalCreateStore(rootReducer);

class App extends Component {
    constructor(props) {
        super(props);
        
        this.solution ='';
        this.state = {
            solutionArr: [],
            
        };

    }
    componentDidMount(){
    }

    render() {

        return (
                <BrowserRouter>
                  
                            <Switch>
                                <Route exact path="/" component={Home}/>
                                <Route path="/classic" component={() => <Play img={classic} level={1} name={'CLASSIC \n SUDOKU'}  store={store} /> }/>
                                <Route path="/xsudoku" component={() => <Play img={xso} level={2} name={'XSUDOKU'}  store={store}  /> }/>
                                <Route path="/winsudoku" component={() => <Play img={win}  level={3} name={'WINSUDOKU'}  store={store} /> }/>
                                <Route path="/jigsaw" component={() => <Jigsaw  img={jig}  level={4} name={'JIGSAW  SUDOKU'}  store={store} /> }/>
                                <Route path="/killer" component={() => <Killer  img={killer}  level={5} name={'KILLER  SUDOKU'}  store={store} /> }/>
                            </Switch>
                       
                </BrowserRouter>
        );
    }
}

export default App


