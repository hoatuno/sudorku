import React, {
	Component
} from 'react';
// import '../../sudoku.css';
 import './jigsaw.css'
import {
	isSolvable,
	isComplete,
	input,
	inputjs
} from '../../ultils/sudoku';
import {
	solve,
	clear,
	undo,
	show,
	hide,
	inputToFile,
	inputToColor
} from '../../actions/actions';
import {
	w3cwebsocket as W3CWebSocket
} from 'websocket'

import Footer from '../../pages/footer';
import Gridjigsaw from '../../components/Gridjigsaw';
import Gridkiller from '../../components/Gridkiller';
import { split } from 'lodash';
const server = 'wss://secret-retreat-74152.herokuapp.com/';
const client = new W3CWebSocket(server);

const initarr = [
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0]
]
const clarr = [
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9]
];


var tmpInput = '';
const jigsawcolor=['#ff9999', '#e699ff', '#ffeb99', ' #ccff99',' #80b3ff', '#d9b38c', '#ffb3cc', '#84e1e1', '#94b8b8'];	

class Killer extends Component {

    constructor(props) {
		super(props);
		this.solution = '';
		this.state = {
			solutionArr: initarr,
			input: '',
            timesouton: '',
            color:'',
            arrColor : [
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
		};
	}

	convertSolution = (solution) => {
		var arr = [
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],	
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0]
		];

		let i = 0,
			j = 0,
			t = 0;
			// console.log(solution)
		let tmpSolution = solution.split(' ');
		
		//  console.log(tmpSolution)
		tmpSolution.map((item,index) => {
			let number = item;
			if (number !== '' ) {
				{
					// console.log(number, i, j)
					arr[i][j] = parseInt(number);
				}
				j++;
				if (j === 9) {
					i++;
					j = 0;
				}
				if(i===9) i=0;
			}
		})
		return arr;
		
	}
	showFile = async (e) => {
		e.preventDefault()
		const reader = new FileReader()
		const {store}= this.props
		reader.onload = async (e) => { 
		  const text = (e.target.result)
		  console.log('input'+ text)
		  let tmp = text.toString().split('\n');
		//    this.setState({input: '4 \n' +text})
		 
		  tmpInput = ''
		  let textinput=''
		  let colorinput=''
		  let i=0;
		  tmp.map((item, index) => {
			  //   if (index !== 0)
			  tmpInput += ` ${item}`;
			  i++;
			  if(i<=9)textinput+= ` ${item}`;
			  if(i>10)colorinput+= ` ${item}`;
			})

		   store.dispatch(inputToFile(this.convertSolution(textinput)))
		   store.dispatch(inputToColor(this.convertSolution(colorinput)))

		   const {
			   grid,
			   Gridcolor
		   } = store.getState();
		   this.setState({input: '4 \n' +inputjs(grid) + inputjs(Gridcolor)})
		};

		reader.readAsText(e.target.files[0])
		// this.forceUpdate();
		
	  }
	
	componentDidMount() {
		client.onopen = () => {
			console.log('WebSocket Client Connected');
		};
		this.unsubscribe = this.props.store.subscribe(() => {
			this.forceUpdate();
			
		})
		client.onmessage = (message) => {
			const text = (message.data)
			let tmp = text.toString().split('\n');
			tmpInput = ''
			console.log(tmp)
			
			tmp.map((item, index) => {
				if (index !== 0)
				tmpInput += ` ${item}`;
			})
			if(tmpInput===' -1'){ alert('This Sudoku is NOT solvable'); tmpInput='';}

			
			this.solution= tmpInput

			// console.log("data"+stringdata)
			 this.setState({timesolotion: tmp[0]!=='OK'? tmp[0] :''})
			// stringdata= stringdata.slice(1)
			this.setState ({solutionArr: this.convertSolution(this.solution)})
		};

	}

	componentWillUnmount() {
		this.unsubscribe();
		const {
			store
		} = this.props

		store.dispatch(clear())
		store.dispatch(inputToColor(clarr))
		store.dispatch(hide())
		
	}

	clearOnClick = () => {
		const {
			store
		} = this.props
		store.dispatch(clear())
		store.dispatch(inputToColor(clarr))
		this.setState({
			solutionArr: initarr,
			color: '',
			timesolotion: '',
			TFcolor:true
		})
			

	}
	showOnClick = ()=>{
		const {	store	} = this.props
		const {	status	} = store.getState();
		const {	isShow	} = status;
		if(!isShow) store.dispatch(show())
		else store.dispatch(hide())
	}
	solveOnclick = async (e) => {
		const {store	} = this.props;
		const {
			grid,
			status,
			Gridcolor
		} = store.getState();
			// console.log(this.state.input)
			  console.log( this.props.level + inputjs(grid) + inputjs(Gridcolor))
			if (true) {
				client.send(JSON.stringify({
					type: 'request',
					data: '5 \n' +inputjs(grid) + inputjs(Gridcolor)
					
					}))
				
				store.dispatch(solve())
				
			}
			// else alert('This Sudoku is NOT solvable');
		
		

	}
    pickcolor =(colorp) =>{
		this.setState({TFcolor: true})
        this.setState({color: colorp})
		
	}

		
	render() {
		const {store, img	} = this.props;
		const {
			grid,
			status,
			Gridcolor
		} = store.getState();
		const {
			isSolved,
			isShow
		} = status;
		
      return (

	<div className="" >
		<div className=' cent jigleft col'>
			<div  className="row">
                <div className='tableOfColor'>
                <div style={{backgroundColor: jigsawcolor[0]}} className="node" onClick={ () => this.pickcolor(0)}></div>
                <div style={{backgroundColor: jigsawcolor[1]}} className="node" onClick={ () => this.pickcolor(1)}></div>
                <div style={{backgroundColor: jigsawcolor[2]}} className="node" onClick={ () => this.pickcolor(2)}></div>
                <div style={{backgroundColor: jigsawcolor[3]}} className="node" onClick={ () => this.pickcolor(3)}></div>
                <div style={{backgroundColor: jigsawcolor[4]}} className="node" onClick={ () => this.pickcolor(4)}></div>
                <div style={{backgroundColor: jigsawcolor[5]}} className="node" onClick={ () => this.pickcolor(5)}></div>
                <div style={{backgroundColor: jigsawcolor[6]}} className="node" onClick={ () => this.pickcolor(6)}></div>
                <div style={{backgroundColor: jigsawcolor[7]}} className="node" onClick={ () => this.pickcolor(7)}></div>
                <div style={{backgroundColor: jigsawcolor[8]}} className="node" onClick={ () => this.pickcolor(8)}></div>
                <div style={{backgroundColor: "#555555"}} className="node" onClick={ () => this.setState({TFcolor: false})}><p className= "check">✔</p></div>


                </div>
			<body className="play">
			<Gridkiller grid={grid} Gridcolor={Gridcolor} status={status} level={this.props.level} color={this.state.color}
			answer={this.state.solutionArr}  solved={this.state.solved} TFcolor={this.state.TFcolor} {...this.props}  />
			</body>

			<body className="playin"> 
			<div className= 'playgame popup '  >
				<h1 alt='See more rules of game' className=''onClick={() => this.showOnClick() } >  {this.props.name} </h1>
				<span className= {!isShow?"popuptex " : "popuptex show " } >
					<img className= "imghowtop"src={img}  alt="su"/>
				</span>
			</div>
			<div className="">
				
				
				<button
						className='undo select'
						disabled={window.gridHistory && !window.gridHistory.length}
						onClick={() => store.dispatch(undo()) }
				>
					⤺ Undo
				</button>
				<button
					className='clear select'
					onClick={ this.clearOnClick }
				>
					⟲ Clear
				</button>
			</div>
			<div className=''>
			
				<button
					className='check select'
					disabled={isSolved}
					onClick={() => {
						if (isSolvable(grid)) {
							if (isComplete(grid)) {
								return alert('Congratulations, you solved it!!');
							}
							alert('This Sudoku is solvable, keep going !!');							
						} else {							
							alert('This Sudoku is NOT solvable');
						}					
					}}
					>
					Check
				</button>
				<button	className='solve select' 					
						onClick={ this.solveOnclick}
				>
						Solve
				</button>			
			</div>
			<div className="cent"> 
				<input type="file" name="file" id="file" class="inputfile"  onChange={this.showFile}/>
				<label for="file">Open file</label>
			</div>
			<div>	
				<h2>Load time: {this.state.timesolotion? this.state.timesolotion +'s' : '' } </h2>		
			</div>	
			</body>
			
			</div>
			<div  className='footerleft'> <Footer/>	</div>
			
		</div>
	</div>

      );
}
}
export default  Killer;

