export function inputValue(row, col, val) {
	return {
		type: 'INPUT_VALUE',
		row,
		col,
		val
	};
}
export function inputColorMouse(row, col, val) {
	return {
		type:'INPUT_COLORMOUSE',
		row,
		col,
		val
	};
}

export function inputToFile(grid) {
	return {
		type: 'INPUT_FILE',
		grid,
	};
}
export function inputToColor(gridcolor) {
	return {
		type: 'INPUT_COLOR',
		gridcolor,
	};
}

export function check(row, col, val) {
	return {
		type: 'CHECK',
	};
}
export function solve() {
	return {
		type: 'SOLVE',


	};
}

export function clear() {
	return {
		type: 'CLEAR'
	};
}


export function undo() {
	return {
		type: 'UNDO'
	};
}
export function show() {
	return {
		type: 'SHOW'
	};
}
export function hide() {
	return {
		type: 'HIDE'
	};
}

