import React, {Component} from 'react';
import { inputValue } from '../actions/actions';


class Box extends Component {
	constructor(props) {
		super(props);
		this.state = {
			color: 'white'
		}
	}
	

	componentWillMount() {
		const {val} = this.props;
		this.setState({isFixed: val ? true : false});
		
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.val !== this.props.val;
	}
	handleChange = (e) => {
		
		const {row, col, store} = this.props;
		const range = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		const val = parseInt(e.target.value);
		const isDeleted = e.target.value === '';

		//update state

		if (range.indexOf(val) > -1 || isDeleted) {
			store.dispatch(inputValue(row, col, isDeleted ? 0 : val));
		}
	}
	// var arr=[0,1,2,3,4,5,6,7,8]
	
	BoxContent = () => {
		const {row, col, val, level, valgrid } = this.props;
		let Xsudoku = ""
		let basic =''
		let win =''

		const winarr = [
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 1, 1, 1, 0, 1, 1, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0]
		];

	
		if (winarr[row][col]) win='win'
		if (row === col || row ===-col+8)Xsudoku = 'xsu'
		
		let suarr=['',basic, Xsudoku, win, '']

		return (
			<td >
				<input 
					ref='input'
					// style={  {backgroundColor: suarr[level] }}
					className= {  valgrid ? 'isInput '+suarr[level] : ''+ suarr[level] }
					value={val ? val : ''}
					onChange={this.handleChange}
					
				/>
					
			</td>
		);
	}

	render() {
		return this.BoxContent()
	}
};

export default Box;
