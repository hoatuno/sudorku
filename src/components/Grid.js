import React, {Component} from 'react';
import Box from './Box';
import '../Games/game123/play.css'
/* Grid Component */
class Grid extends Component{
	render() {
		const {grid, status, level} = this.props;
		const {isSolved,isEdited,isInput } = status;
		const renderBox = (row, val, col) => {
			return (
				<Box
					key={col}
					row={row}
					col={col}
					val={!isSolved ? val : this.props.answer[row][col]}
					// val={val}
					level={level}
					// val={val}
					isSolved={isSolved}
					isEdited={isEdited}
					isInput={isInput}
					valgrid={val}
					{...this.props}

				/>
			);
		};
		const renderRow = (vals, row) => {
			return (
				<tr key={row} className= '' >
					{vals.map(renderBox.bind(this, row))}
				</tr>
			);			
		};
		
		return (
			<table>
				<tbody className= 'ccc'>
					{grid.map(renderRow)}
				</tbody>
			</table>

		);
	}
};

export default Grid;
