import React, {Component} from 'react';

import '../Games/jigsaws/jigsaw.css';
import Boxkiller from './boxkiller';


/* Grid Component */
class Gridkiller extends Component{
	render() {
		const {grid, status, level, color,answer, TFcolor} = this.props;
		const {isSolved,isEdited,isInput } = status;
		const renderBox = (row, val, col) => {
			return (
				<Boxkiller
					key={col}
					row={row}
                    col={col}
                    color={color}
					val={!isSolved ? val : answer[row][col]}
					level={level}
					isSolved={isSolved}
					isEdited={isEdited}
					isInput={isInput}
					valgrid={val}
					Gridcolor={this.props.Gridcolor}
					TFcolor= {TFcolor}
					{...this.props}

				/>
			);
		};
		const renderRow = (vals, row) => {
			return (
				<tr key={row} className="noneboder">
					{vals.map(renderBox.bind(this, row))}
				</tr>
			);			
		};
		
		return (
			<table>
				<tbody  className="noneboder">
					{grid.map(renderRow)}
				</tbody>
			</table>

		);
	}
};

export default Gridkiller;
