import React, {Component} from 'react';
import { inputValue, inputColorMouse } from '../actions/actions';


class Boxjigsaw extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// color: 'white'
			bun:false
		}
	}
	
	componentWillMount() {
		const {val} = this.props;
		this.setState({isFixed: val ? true : false});
		
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.val !== this.props.val || nextProps.Gridcolor !== this.props.Gridcolor;
	}
	handleChange = (e) => {
		
		const {row, col, store} = this.props;
		const range = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		const val = parseInt(e.target.value);
		const isDeleted = e.target.value === '';

		//update state

		if (range.indexOf(val) > -1 || isDeleted) {
			store.dispatch(inputValue(row, col, isDeleted ? 0 : val));
		}
	}
	handleOn = () =>{
		this.setState({bun:true})
		console.log(this.state.bun)

	}
	handleOff =() =>{
		this.setState({bun:true})
		console.log(this.state.bun)
	}
	handleColor = () =>{
		console.log(this.state.bun)
		const {row, col, store, color, TFcolor} = this.props;
		const val = color;
		if(TFcolor)store.dispatch(inputColorMouse(row, col, val));
	}
	
	BoxContent = () => {
		const {row, col, val, valgrid,Gridcolor } = this.props;
		const jigsawcolor=['#ff8080','#ffccb3','#ffff66','#85e085','#66c2ff','#80ffdf','#ffb3d9','#e699ff','#cccccc'];		let check = true;
		Gridcolor.map((row) => row.map(e => {if (e !== 0) check = false;}))
		let jigsaw =  jigsawcolor[Gridcolor[row][col]];
		return (
			<td >
				<input 
					ref='input'
					style={  {backgroundColor: jigsaw }}
					className= {  valgrid ? 'isInput ' : '' }
					value={val ? val : ''}
					onChange={this.handleChange}
					onMouseDown     ={this.handleOn}
					onMouseDown ={this.handleColor}
					
				/>
					
			</td>
		);
	}

	render() {
		return this.BoxContent()
	}
};

export default Boxjigsaw;
