import React, {
	Component
} from 'react';
import '../../sudoku.css';
import './play.css'
import {
	isSolvable,
	isComplete,
	input
} from '../../ultils/sudoku';
import Grid from '../../components/Grid';
import {
	solve,
	clear,
	undo,
	show,
	hide,
	inputToFile
} from '../../actions/actions';
import {
	w3cwebsocket as W3CWebSocket
} from 'websocket'

import Footer from '../../pages/footer';
const server = 'wss://secret-retreat-74152.herokuapp.com/';
const client = new W3CWebSocket(server);

const initarr = [
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0]
]

var tmpInput = '';

class Play extends Component {

	constructor(props) {
		super(props);
		this.solution = '';
		this.state = {
			solutionArr: initarr,
			input: '',
			timesouton: ''
		};
	}

	convertSolution = (solution) => {
		let arr = [
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0]
		];

		let i = 0,
			j = 0,
			t = 0;
		while (solution[t]) {
			let number = solution[t];
			if (number !== ' ') {
				arr[i][j] = parseInt(solution[t], 10);
				j++;
				if (j === 9) {
					i++;
					j = 0;
				}
			}

			t++;
		}
		this.setState({
			solutionArr: arr
		})
		return arr;
		
	}
	showFile = async (e) => {
		e.preventDefault()
		const reader = new FileReader()
		const {store}= this.props
		reader.onload = async (e) => { 
		  const text = (e.target.result)
		  let tmp = text.toString().split('\n');
		  tmpInput = ''
		  
		  tmp.map((item, index) => {
			  if (index !== 0)
			  tmpInput += ` ${item}`;
			})
		  console.log(tmpInput)
		  console.log(this.convertSolution(tmpInput))
		  store.dispatch(inputToFile(this.convertSolution(tmpInput)))
		};
		reader.readAsText(e.target.files[0])
		
	  }
	componentDidMount() {
		client.onopen = () => {
			console.log('WebSocket Client Connected');
		};
		this.unsubscribe = this.props.store.subscribe(() => {
			this.forceUpdate();
			console.log(this.props)
		})
		client.onmessage = (message) => {
			
			const text = (message.data)
			let tmp = text.toString().split('\n');
			tmpInput = ''
			console.log(tmp)
			if(tmp='-1') alert('This Sudoku is NOT solvable')
			else{
			tmp.map((item, index) => {
				if (index !== 0)
				tmpInput += ` ${item}`;
			})
			console.log(tmpInput)
			this.solution= tmpInput

			// console.log("data"+stringdata)
			 this.setState({timesolotion: tmp[0]!=='OK'? tmp[0] :''})
			// stringdata= stringdata.slice(1)
			
			this.convertSolution(this.solution)
		}
			
		};
	}

	componentWillUnmount() {
		this.unsubscribe();
		const {
			store
		} = this.props
		store.dispatch(clear())
		store.dispatch(hide())
	}

	clearOnClick = () => {
		const {
			store
		} = this.props
		store.dispatch(clear())
		this.setState({
			solutionArr: initarr
		})
		this.setState({timesolotion:''})
			

	}
	showOnClick = ()=>{
		const {	store	} = this.props
		const {	status	} = store.getState();
		const {	isShow	} = status;
		if(!isShow) store.dispatch(show())
		else store.dispatch(hide())
	}

	
		
	render() {
		const {store,level, img	} = this.props;
		const {
			grid,
			status
		} = store.getState();
		const {
			isSolved,
			isShow
		} = status;
		console.log(this.state.input)
      return (

	<div className="" >
		<div className=' cent left col'>
			<div  className="row">
			<body className="play">
			<Grid grid={grid}  status={status} level={this.props.level} answer={this.state.solutionArr}  solved={this.state.solved} {...this.props}  />
			</body>

			<body className="playin"> 
			<div className= 'playgame popup'  >
				<h1 alt='See more rules of game' className=''onClick={() => this.showOnClick() } >  {this.props.name} </h1>
				<span className= {!isShow?"popuptext " : "popuptext show " } >
					<img className= "imghowtop"src={img}  alt="su"/>
				</span>
			</div>
			<div className="">
				
				
				<button
						className='undo select'
						disabled={window.gridHistory && !window.gridHistory.length}
						onClick={() => store.dispatch(undo()) }
				>
					⤺ Undo
				</button>
				<button
					className='clear select'
					onClick={ this.clearOnClick }
				>
					⟲ Clear
				</button>
			</div>
			<div className=''>
			
				<button
					className='check select'
					disabled={isSolved}
					onClick={() => {
						if (isSolvable(grid)) {
							if (isComplete(grid)) {
								return alert('Congratulations, you solved it!!');
							}
							alert('This Sudoku is solvable, keep going !!');							
						} else {							
							alert('This Sudoku is NOT solvable');
						}					
					}}
					>
					Check
				</button>
				<button	className='solve select' 					
						onClick={() => {
							if (isSolvable(grid)) {
								client.send(JSON.stringify({
									type: 'request',
									data: input(grid,level)
									
									}))
								
								store.dispatch(solve())
							}
							else alert('This Sudoku is NOT solvable');
						}
						}
				>
						Solve
				</button>			
			</div>
			<div className="playrow"> 
				<input type="file" name="file" id="file" class="inputfile"  onChange={this.showFile}/>
				<label for="file">Open file</label>
				<h2>Execution time: {this.state.timesolotion? this.state.timesolotion +'s' : '' } </h2>		
			</div>	
			</body>
			
			</div>
			<div  className='footerleft'> <Footer/>	</div>
			
		</div>
	</div>

      );
}
}
export default  Play;

