import React, {
	Component
} from 'react';
// import '../../sudoku.css';
 import './jigsaw.css'
import {
	isSolvable,
	isComplete,
	input
} from '../../ultils/sudoku';
import {
	solve,
	clear,
	undo,
	show,
	hide,
	inputToFile,
	inputToColor
} from '../../actions/actions';
import {
	w3cwebsocket as W3CWebSocket
} from 'websocket'

import Footer from '../../pages/footer';
import Gridjigsaw from '../../components/Gridjigsaw';
const server = 'wss://secret-retreat-74152.herokuapp.com/';
const client = new W3CWebSocket(server);

const initarr = [
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0]
]

var tmpInput = '';
const jigsawcolor=['#ff8080','#ffccb3','#ffff66','#85e085','#66c2ff','#80ffdf','#ffb3d9','#e699ff','#cccccc']

class Jigsaw extends Component {

    constructor(props) {
		super(props);
		this.solution = '';
		this.state = {
			solutionArr: initarr,
			input: '',
            timesouton: '',
            color:'',
            arrColor : [
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0]
            ]
		};
	}

	convertSolution = (solution) => {
		let arr = [
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0]
		];

		let i = 0,
			j = 0,
			t = 0;
		while (solution[t]) {
			let number = solution[t];
			if (number !== ' ') {
				
				arr[i][j] = parseInt(solution[t], 10);
				j++;
				if (j === 9) {
					i++;
					j = 0;
				}
			}

			t++;
		}
		// this.setState({
		// 	solutionArr: arr
		// })
		return arr;
		
	}
	showFile = async (e) => {
		e.preventDefault()
		const reader = new FileReader()
		const {store}= this.props
		reader.onload = async (e) => { 
		  const text = (e.target.result)
		  let tmp = text.toString().split('\n');
		  this.setState({input: '4 \n' +text})
		 
		  tmpInput = ''
		  let textinput=''
		  let colorinput=''
		  
		  
		  tmp.map((item, index) => {
			//   if (index !== 0)
			  tmpInput += ` ${item}`;
			})
		  for(let i=1;i<163;i++){
			  textinput+=tmpInput[i];
			   
		  }
		  for(let i=164;i<326;i++){
			colorinput+=tmpInput[i];
			 
		}

		
		   store.dispatch(inputToFile(this.convertSolution(textinput)))
		   store.dispatch(inputToColor(this.convertSolution(colorinput)))
		};
		reader.readAsText(e.target.files[0])
		// this.forceUpdate();
		
	  }
	componentDidMount() {
		client.onopen = () => {
			console.log('WebSocket Client Connected');
		};
		this.unsubscribe = this.props.store.subscribe(() => {
			this.forceUpdate();
			
		})
		client.onmessage = (message) => {
			const text = (message.data)
			let tmp = text.toString().split('\n');
			tmpInput = ''
			console.log(tmp)
			
			tmp.map((item, index) => {
				if (index !== 0)
				tmpInput += ` ${item}`;
			})
			if(tmp='-1') alert('This Sudoku is NOT solvable');
			
			this.solution= tmpInput

			// console.log("data"+stringdata)
			 this.setState({timesolotion: tmp[0]!=='OK'? tmp[0] :''})
			// stringdata= stringdata.slice(1)
			this.setState ({solutionArr: this.convertSolution(this.solution)})

		
			
			
		};
	}

	componentWillUnmount() {
		this.unsubscribe();
		const {
			store
		} = this.props
		store.dispatch(clear())
		store.dispatch(hide())
	}

	clearOnClick = () => {
		const {
			store
		} = this.props
		store.dispatch(clear())
		store.dispatch(inputToColor(initarr))
		this.setState({
			solutionArr: initarr,
			color: '',
			timesolotion: ''
		})
			

	}
	showOnClick = ()=>{
		const {	store	} = this.props
		const {	status	} = store.getState();
		const {	isShow	} = status;
		if(!isShow) store.dispatch(show())
		else store.dispatch(hide())
	}
    pickcolor =(colorp) =>{
         this.setState({color: colorp})
		
    }
	
		
	render() {
		const {store,level, img	} = this.props;
		const {
			grid,
			status,
			Gridcolor
		} = store.getState();
		const {
			isSolved,
			isShow
		} = status;
	
      return (

	<div className="" >
		<div className=' cent jigleft col'>
			<div  className="row">
                <div className='tableOfColor'>
                <div style={{backgroundColor: jigsawcolor[0]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[0])}></div>
                <div style={{backgroundColor: jigsawcolor[1]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[1])}></div>
                <div style={{backgroundColor: jigsawcolor[2]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[2])}></div>
                <div style={{backgroundColor: jigsawcolor[3]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[3])}></div>
                <div style={{backgroundColor: jigsawcolor[4]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[4])}></div>
                <div style={{backgroundColor: jigsawcolor[5]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[4])}></div>
                <div style={{backgroundColor: jigsawcolor[6]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[4])}></div>
                <div style={{backgroundColor: jigsawcolor[7]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[4])}></div>
                <div style={{backgroundColor: jigsawcolor[8]}} className="node" onClick={ () => this.pickcolor(jigsawcolor[4])}></div>




                </div>
			<body className="play">
			<Gridjigsaw grid={grid} Gridcolor={Gridcolor} status={status} level={this.props.level} color={this.state.color} answer={this.state.solutionArr}  solved={this.state.solved} {...this.props}  />
			</body>

			<body className="playin"> 
			<div className= 'playgame popup '  >
				<h1 alt='See more rules of game' className=''onClick={() => this.showOnClick() } >  {this.props.name} </h1>
				<span className= {!isShow?"popuptex " : "popuptex show " } >
					<img className= "imghowtop"src={img}  alt="su"/>
				</span>
			</div>
			<div className="">
				
				
				<button
						className='undo select'
						disabled={window.gridHistory && !window.gridHistory.length}
						onClick={() => store.dispatch(undo()) }
				>
					⤺ Undo
				</button>
				<button
					className='clear select'
					onClick={ this.clearOnClick }
				>
					⟲ Clear
				</button>
			</div>
			<div className=''>
			
				<button
					className='check select'
					disabled={isSolved}
					onClick={() => {
						if (isSolvable(grid)) {
							if (isComplete(grid)) {
								return alert('Congratulations, you solved it!!');
							}
							alert('This Sudoku is solvable, keep going !!');							
						} else {							
							alert('This Sudoku is NOT solvable');
						}					
					}}
					>
					Check
				</button>
				<button	className='solve select' 					
						onClick={() => {
							if (isSolvable(grid)) {
								client.send(JSON.stringify({
									type: 'request',
									data: this.state.input
									
									}))
								
								store.dispatch(solve())
								
							}
							else alert('This Sudoku is NOT solvable');
						}
						}
				>
						Solve
				</button>			
			</div>
			<div className="playrow"> 
				<input type="file" name="file" id="file" class="inputfile"  onChange={this.showFile}/>
				<label for="file">Open file</label>
				<h2>Load time: {this.state.timesolotion? this.state.timesolotion +'s' : '' } </h2>		
			</div>	
			</body>
			
			</div>
			<div  className='footerleft'> <Footer/>	</div>
			
		</div>
	</div>

      );
}
}
export default  Jigsaw;

