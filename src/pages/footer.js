import React, {Component} from 'react'
import logo from '../image/logo.png'
import math from '../image/math.png'
import '../../src/sudoku.css'

export default class Footer extends Component {
    render() {
        return (
            <div className= "footerContainer" >
            {/* Footer*/}
          <footer className='col'>
              <div className="" >
                <img className="imgOutro" src={math} alt="su"/> 
                <img className="imgOutro" src={logo} alt="su"/> 
              </div>
              <div className= "textContainer"> 
              <p>Copyright © 2020 - This is a product in collaboration of </p>
              <p>Institute of Mathematics (Vietnam Academy of Science and Technology) and ORLab (Phenikaa University). </p></div>
          </footer>
            </div>
        );
    }
}
