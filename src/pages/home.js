import React, { Component } from 'react';
import { Link } from "react-router-dom";
import i1 from '../image/i1.png'
import i2 from '../image/i2.png'
import i3 from '../image/i3.png'
import i4 from '../image/i4.png'
import i5 from '../image/i5.png'
// import i5 from '../image/i5.png'
import '../sudoku.css';
import Footer from './footer';

class Home extends Component {
    render() {
        return (
            <div>
                <div  className="bodyContainer">
                    <body>
                        <div className=''>
                            <h1 className='cent homepage'>SUDOKU GAME</h1>
                        </div>
                        <div className="col">
                            <div  className="row">
                                <Link className="linkBS" to ="/xsudoku"> <div className='card'><img className="imgCard" src={i2} alt="su" /> <h3> X-SUDOKU</h3></div></Link>
                                <Link className="linkBS" to ="/classic"> <div className='card'><img className="imgCard" src={i1} alt="su"  /> <h3 className="maxw"> CLASSIC SUDOKU</h3> </div></Link>
                                <Link className="linkBS" to ="/winsudoku"> <div className='card'><img className="imgCard" src={i3} alt="su" /> <h3> WINSUDOKU</h3></div></Link>
                            </div>
                            <div  className="row">
                                <Link className="linkBS" to ="/jigsaw"> <div className='card'><img className="imgCard" src={i4}  alt="su"/> <h3 className="maxw "> JIGSAW SUDOKU</h3></div></Link>
                                <Link className="linkBS" to ="/killer"> <div className='card'><img className="imgCard" src={i5}  alt="su"/> <h3 className=" maxw"> KILLER SUDOKU</h3></div></Link>
                            </div>
                        </div>
                    </body>
                </div>
                <Footer/>
            </div>
        );
    }
}
export default Home
