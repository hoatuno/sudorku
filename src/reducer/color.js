import cloneDeep from 'lodash/cloneDeep';
const initialState = [
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9],
	[9, 9, 9, 9, 9, 9, 9, 9, 9]
];

window.gridHistory = window.gridHistory || [];
export function Gridcolor(state = cloneDeep(initialState), action) {
	switch (action.type) {
		case 'INPUT_COLOR':
            let {gridcolor}= action;
            // console.log(gridcolor);
			return (gridcolor)
		case 'INPUT_COLORMOUSE':
				let {row, col, val} = action;
				let changedRow = [
					...state[row].slice(0, col),
					val,
					...state[row].slice(col + 1)
				]; // Omit using splice since it mutates the state
				window.gridHistory.push(state);		
				return [
					...state.slice(0, row),
					changedRow,
					...state.slice(row + 1)
				];

		default:
			return state;
	}
}