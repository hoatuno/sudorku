import cloneDeep from 'lodash/cloneDeep';


const initialState = [
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0, 0]
];


window.gridHistory = window.gridHistory || [];

export function grid(state = cloneDeep(initialState), action) {
	switch (action.type) {
		case 'INPUT_VALUE':
			let {row, col, val} = action;
			let changedRow = [
				...state[row].slice(0, col),
				val,
				...state[row].slice(col + 1)
			]; // Omit using splice since it mutates the state
			window.gridHistory.push(state);		
			return [
				...state.slice(0, row),
				changedRow,
				...state.slice(row + 1)
			];
		case 'INPUT_FILE': 
			let {grid}= action;
			return (grid)
		case 'CLEAR':
			window.gridHistory = [];
			return cloneDeep(initialState);
		
		case 'UNDO':
			let lastState = window.gridHistory.splice(window.gridHistory.length - 1, 1);
			return lastState[0]; 

		default:
			return state;
	}
}
