import { combineReducers } from 'redux';
import { grid } from './grid';
import { status } from './status';
import { Gridcolor } from './color';

const rootReducer = combineReducers({
	grid,
	Gridcolor,
	status
});

export default rootReducer;
