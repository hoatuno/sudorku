import cloneDeep from 'lodash/cloneDeep';
import { default as extend } from 'lodash/assignIn';

const initialState = {
	isSolved: false,
	isEdited: false,
	isShow: false,

};

export function status(state = cloneDeep(initialState), action) {
	switch (action.type) {
		case 'INPUT_VALUE':
			return extend({}, state, {isEdited: true}, );
		case 'SOLVE':
			return extend({}, state, {isSolved: true});
		case 'CLEAR':
			return extend({}, state, {isSolved: false, isEdited: false});
		case 'UNDO':
			if (!window.gridHistory.length) {
				return extend({}, state, {isEdited: false } ); 
			}return state;
		case 'CHECK':
			return extend({}, state, {isConflict: true});
		case 'SHOW': 
			return extend({}, state, {isShow: true});
		case 'HIDE':
			return extend({}, state, {isShow: false});
		default:
			return state;
	}
}
