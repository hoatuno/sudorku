import cloneDeep from 'lodash/cloneDeep';
import flatten from 'lodash/flatten';

const getRow = (grid, rowNum) => {

    return grid[rowNum];
}
const getCol = (grid, colNum) => {

    return grid.map((row) => row[colNum]);
}
const getSquare = (grid, rowNum, colNum) => {

    let rowStart = rowNum - (rowNum % 3); // uppermost row index of the box
    let colStart = colNum - (colNum % 3); // leftmost col index of the box
    let result = [];
    for (let row = 0; row < 3; row++) {
        result[row] = result[row] || [];
        for (let col = 0; col < 3; col++) {
            result[row].push(grid[rowStart + row][colStart + col]);
        }
    }
    return flatten(result);
}

function checkRow(grid) {
    for (let row = 0; row < 9; row++) {
        let arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        let rowl = getRow(grid, row);
        for (let i = 0; i <= 9; i++) {
            arr[rowl[i]] += 1;
        }
        // console.log(arr);
        for (let i = 1; i <= 9; i++) {
            if (arr[i] >= 2) {

                return false;
            }
        }
    }
    return true;

}

function checkCol(grid) {
    for (let row = 0; row < 9; row++) {
        let arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        let rowl = getCol(grid, row);
        for (let i = 0; i <= 9; i++) {
            arr[rowl[i]] += 1;
        }
        //console.log(arr);
        for (let i = 1; i <= 9; i++) {
            if (arr[i] >= 2) {

                return false;
            }
        }
    }
    return true;
}

function checkSquare(grid) {
    for (let row = 0; row < 9; row++) {
        for (let col = 0; col < 9; col++) {
            let arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            let rowl = getSquare(grid, row, col);
            for (let i = 0; i <= 9; i++) {
                arr[rowl[i]] += 1;
            }
            //console.log(rowl);
            for (let i = 1; i <= 9; i++) {
                if (arr[i] >= 2) {

                    return false;
                }
            }
        }
    }
    return true;
}

export const solver = (grid, rowNum = 0, colNum = 0) => {
    return checkRow(grid) && checkCol(grid) && checkSquare(grid)
}
export const solverjs = (grid, rowNum = 0, colNum = 0) => {
    return  checkCol(grid) && checkSquare(grid)
}
export const isSolvable = (grid) => {
    let clonedGrid = cloneDeep(grid);
    console.log(clonedGrid)
    return solver(clonedGrid);
}
export const isSolvablejs = (grid) => {
    let clonedGrid = cloneDeep(grid);
    console.log(clonedGrid)
    return solverjs(clonedGrid);
}
export const input = (grid, level) => {
    let clonedGrid = cloneDeep(grid);
    // console.log(clonedGrid)
    let arr = clonedGrid
    let res = '';
    res += level.toString();
    res += '\n'
    for (let i = 0; i < 9; i++) {
        let col = arr[i];
        for (let j = 0; j < 9; j++) {
            res += col[j].toString();
            res += ' ';
        }
        res += '\n'

    }
    // if (level === 4) res += ' 1 1 1 2 1 3 1 4 2 1 2 2 2 3 2 4 3 3 \n 3 1 3 2 4 1 4 2 5 1 6 1 7 1 7 2 8 1\n 1 5 1 6 1 7 1 8 1 9 2 6 2 7 2 8 3 7 \n 2 5 3 5 3 6 4 6 4 7 4 8 5 7 5 8 6 7 \n 3 4 4 4 4 5 5 4 5 5 5 6 6 5 6 6 7 6 \n 4 3 5 2 5 3 6 2 6 3 6 4 7 4 7 5 8 5 \n 7 3 8 2 8 3 8 4 9 1 9 2 9 3 9 4 9 5 \n 2 9 3 8 3 9 4 9 5 9 6 8 6 9 7 8 7 9 \n 7 7 8 6 8 7 8 8 8 9 9 6 9 7 9 8 9 9 '

    return res;
}
export const inputjs = (grid) => {
    let clonedGrid = cloneDeep(grid);
    // console.log(clonedGrid)
    let arr = clonedGrid
    let res = ''
    res += '\n'
    for (let i = 0; i < 9; i++) {
        let col = arr[i];
        for (let j = 0; j < 9; j++) {
            res += col[j].toString();
            res += ' ';
        }
        res += '\n'

    }

    return res;
}

export const isComplete = (grid) => {
    let values = flatten(grid);
    let list = {};
    values.map((val) => list[val] = list[val] ? list[val] + 1 : 1);
    delete list['0'];
    var total = Object.keys(list).reduce((total, key) => total + list[key], 0);
    return total === 81 ? true : false;
}